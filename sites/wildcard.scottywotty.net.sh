#!/bin/bash
#

MYHOST=$(uname -n)

KEYFILE=/etc/letsencrypt/live/scottywotty.net/privkey.pem
CRTFILE=/etc/letsencrypt/live/scottywotty.net/fullchain.pem

TIMESTAMP=$(date +"%Y%m%d-%H%M")

function backup_file {
	typeset __bu_FILE
	__bu_FILE="$1"
	[[ -n "$__bu_FILE" ]] || return 1
	[[ -f "$__bu_FILE" ]] || return 0
	/bin/mv "$__bu_FILE" "$__bu_FILE.$TIMESTAMP"
}

if  [[ "$MYHOST" == "cos7" ]]; then
	##
	## Apache cert
	##
	UPDATED=0
	# On cos7, replace the standard linux keys
	KEYDST=/etc/pki/tls/private/localhost.key
	CRTDST=/etc/pki/tls/certs/localhost.crt

	if [[ $KEYFILE -nt $KEYDST ]]; then
		if ! backup_file $KEYDST ; then
			echo "Could not backup $KEYDST"
			exit 1
		fi
		/bin/cp -p $KEYFILE $KEYDST || exit 1
		UPDATED=1
	fi
		
	if [[ $CRTFILE -nt $CRTDST ]]; then
		if ! backup_file $CRTDST ; then
			echo "Could not backup $CRTDST"
			exit 1
		fi
		/bin/cp -p $CRTFILE $CRTDST || exit 1
		UPDATED=1
	fi
	if (( UPDATED == 1 )) ; then
		systemctl restart httpd
	fi


	# Done
	exit 0
fi

		
	

	

