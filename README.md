# Let's Encrypt management.

## Tools

- bin/lem-init - initialize tools
- bin/lem-rewnew - renews and sends out certs.

## Admin

- sbin/lem-update-from-repo - grab a new copy from the repo and make sure the perms are correct.

## Config files

- etc/example.com.cfg.sample - a sample configuration file.
