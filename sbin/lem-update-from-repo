#!/bin/bash
#
# VERSION=1.0
#
# Make sure we are well protected.
umask 077

#
# cd to the top directory and extract there
TOPDIR=/opt/letsencrypt-scripts
if [[ ! -d $TOPDIR ]] ; then
	mkdir -p $TOPDIR || exit 1
	chmod 700 $TOPDIR
fi

# Create a couple of tmp files.  One for this script and one for the tar file.
# We want to make a copy of this script so we don't go wiggy if the git version
# changed.
TMPTAR=""
TMPSCRIPT=""
cleanup() {
	/bin/rm -f $TMPTAR $TMPSCRIPT
}
trap cleanup 0
TMPTAR=$(mktemp /tmp/XXXXXXXX)
TMPSCRIPT=$(mktemp /tmp/XXXXXXXX)

if [[ "$1" != "RUNNING_SECOND_SCRIPT" ]]; then
	/bin/cp $0 $TMPSCRIPT || exit 1
	/bin/bash $TMPSCRIPT RUNNING_SECOND_SCRIPT "$@"
	exit $?
fi
shift

# Grab the latest tar file and extract it
curl -s -o $TMPTAR https://bitbucket.org/gramgersh/letsencrypt-scripts/get/master.tar.gz
# Find the version of this tar file
CURVERS=$(tar tf $TMPTAR | head -1 | sed -e 's/.*letsencrypt-scripts-//' -e 's|/$||')

VERSFILE=$TOPDIR/etc/gitversion.txt
# Find the previous version
PREVVERS=0
if [[ -f $VERSFILE ]]; then
	read PREVVERS < $VERSFILE
fi

# If they're different, extract them.
if [[ "$CURVERS" != "$PREVVERS" ]]; then
	[[ -t 0 ]] && echo "Git versions changed.  Updating."
	tar -C $TOPDIR -xzf $TMPTAR --strip-components=1 --exclude .gitignore --backup=t
	echo "$CURVERS" > $VERSFILE
fi
